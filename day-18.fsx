#r "nuget: FSharpx.Collections"
open FSharpx.Collections

type Value = Literal of int64 | RegisterRef of char
type Instruction =
  | Set of char * Value
  | Add of char * Value
  | Mul of char * Value
  | Mod of char * Value
  | Jgz of Value * Value
  | Snd of Value
  | Rcv of char

type Cpu = {
  Registers: Map<char, int64>
  Program: Instruction list
  PC: int
  Input: Queue<int64>
  Output: int64 option
  SendCount: int
  Waiting: bool
}

let parseInstruction (str: string) =
  let words = str.Split ' '
  let charAt idx = words.[idx].[0]
  let numAt idx = words.[idx] |> int64
  let valueAt idx =
    if words.[idx].[0] |> System.Char.IsLetter then
      RegisterRef (charAt idx)
    else
      Literal (numAt idx)

  match words.[0] with
  | "set" -> Set (charAt 1, valueAt 2)
  | "add" -> Add (charAt 1, valueAt 2)
  | "mul" -> Mul (charAt 1, valueAt 2)
  | "mod" -> Mod (charAt 1, valueAt 2)
  | "jgz" -> Jgz (valueAt 1, valueAt 2)
  | "snd" -> Snd (valueAt 1)
  | "rcv" -> Rcv (charAt 1)
  | _ -> raise <| System.Exception()

let input = System.IO.File.ReadAllLines "day-18-input.txt" |> Array.toList |> List.map parseInstruction
let initialCpu id = {
  Registers = Map ['p', id]
  Program = input
  PC = 0
  Input = Queue.empty
  Output = None
  SendCount = 0
  Waiting = false
}

let getReg reg cpu = cpu.Registers |> Map.tryFind reg |> Option.defaultValue 0L
let setReg reg v cpu = { cpu with Registers = cpu.Registers |> Map.add reg v }
let resolve cpu = function | Literal v -> v | RegisterRef r -> cpu |> getReg r
let step cpu = { cpu with PC = cpu.PC + 1 }

let runCycle cpu =
  let cpu = { cpu with Output = None; Waiting = false }
  match (cpu.Program |> List.item cpu.PC) with
  | Set (x, y) -> cpu |> setReg x (resolve cpu y) |> step
  | Add (x, y) -> cpu |> setReg x (getReg x cpu + resolve cpu y) |> step
  | Mul (x, y) -> cpu |> setReg x (getReg x cpu * resolve cpu y) |> step
  | Mod (x, y) -> cpu |> setReg x (getReg x cpu % resolve cpu y) |> step
  | Jgz (x, _) when resolve cpu x <= 0L -> cpu |> step
  | Jgz (_, y) -> { cpu with PC = cpu.PC + int (resolve cpu y) }
  | Snd x -> { cpu with Output = Some (resolve cpu x); SendCount = cpu.SendCount + 1 } |> step
  | Rcv _ when cpu.Input.IsEmpty -> { cpu with Waiting = true }
  | Rcv x -> { cpu with Input = Queue.tail cpu.Input } |> setReg x (Queue.head cpu.Input) |> step

let rec run cpuA cpuB =
  if cpuA.Waiting && cpuB.Waiting then
    // Deadlock. Return the resulting CPU states.
    cpuA, cpuB
  else
    let sendOutput dest src =
      match src.Output with
      | None -> dest
      | Some x -> { dest with Input = dest.Input |> Queue.conj x }
    run (sendOutput cpuA cpuB |> runCycle) (sendOutput cpuB cpuA |> runCycle)

// Part 1 re-written in terms of the instruction set from part 2.
// Run 2 CPUs. The first has the real program, the other simply accepts input and stores it.
let p1a = initialCpu 0L
let p1b = { p1a with Program = [Rcv 'a'; Jgz (Literal 1L, Literal -1L)] }
printfn "Part 1: %d" (run p1a p1b |> snd |> getReg 'a')
printfn "Part 2: %d" (run (initialCpu 0L) (initialCpu 1L) |> snd |> fun x -> x.SendCount)
