let input = 361527

let dist (x, y) = abs x + abs y

let spiral valGetter = seq {
  let mutable (x, y) = 0, 0
  let mutable l = 1
  let mutable idx = 0
  let mutable vals = Map.empty

  let seek dx dy = seq {
    for _ in [1..l] do
      let value = valGetter idx (x, y) vals
      vals <- vals |> Map.add (x, y) value
      yield (x, y), value
      x <- x + dx
      y <- y + dy
      idx <- idx + 1
  }

  while true do
    if l % 2 = 1 then
      yield! seek 1 0
      yield! seek 0 1
    else
      yield! seek -1 0
      yield! seek 0 -1
    l <- l + 1
}

let incr i _ _ = i + 1
let squareSum i (x, y) vals =
  match i with
  | 0 -> 1
  | _ ->
    let square = [x - 1; x; x + 1] |> List.collect (fun x -> [x, y - 1; x, y; x, y + 1])
    square |> Seq.sumBy (fun c -> vals |> Map.tryFind c |> Option.defaultValue 0)

printfn "Part 1: %d" (spiral incr |> Seq.find (fun (_, v) -> v = input) |> fst |> dist)
printfn "Part 2: %d" (spiral squareSum |> Seq.find (fun (_, v) -> v > input) |> snd)
