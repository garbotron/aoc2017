open System.Text.RegularExpressions

type ProgramTree = {
  Weights: Map<string, int>
  Parents: Map<string, string>
  Children: Map<string, string list>
}

let foldInputLine (tree: ProgramTree) (str: string) =
  let m = Regex.Match(str, @"(?<name>[a-z]+) \((?<weight>[0-9]+)\)( -> (?<children>.*))?")
  let name = m.Groups.["name"].Value
  let tree = { tree with Weights = tree.Weights |> Map.add name (int m.Groups.["weight"].Value) }
  if m.Groups.["children"].Success then
    let children = m.Groups.["children"].Value.Split ", "
    let folder tree child =
      let curChildren = tree.Children |> Map.tryFind name |> Option.defaultValue []
      { tree with
          Parents = tree.Parents |> Map.add child name
          Children = tree.Children |> Map.add name (child :: curChildren) }
    children |> Array.fold folder tree
  else
    tree

let emptyTree = { Weights = Map.empty; Parents = Map.empty; Children = Map.empty }
let input = System.IO.File.ReadAllLines "day-07-input.txt" |> Array.fold foldInputLine emptyTree

let rec findRoot tree program =
  match tree.Parents |> Map.tryFind program with
  | None -> program
  | Some x -> findRoot tree x

let rec weight tree program =
  let localWeight = tree.Weights |> Map.find program
  match tree.Children |> Map.tryFind program with
  | None -> localWeight
  | Some children -> localWeight + (children |> List.sumBy (weight tree))

let rec findImbalanced tree program =
  match tree.Children |> Map.tryFind program with
  | None -> None
  | Some children ->
    // First, check if any of the children are themselves imbalanced.
    match children |> List.choose (findImbalanced tree) |> List.tryHead with
    | Some imbaChild -> Some imbaChild
    | None ->
      let childWeights = children |> List.map (fun x -> x, weight tree x)
      let grouped = childWeights |> List.groupBy snd |> List.map snd
      if grouped.Length = 1 then
        None
      else
        let imbalanced = grouped |> List.find (fun x -> x.Length = 1) |> List.head |> fst
        let balanced = children |> List.find (fun x -> x <> imbalanced)
        let diff = (weight tree balanced) - (weight tree imbalanced)
        Some ((tree.Weights |> Map.find imbalanced) + diff)

let root = findRoot input (input.Parents |> Map.toSeq |> Seq.head |> fst)
printfn "Part 1: %s" root
printfn "Part 2: %d" (root |> findImbalanced input |> Option.get)
