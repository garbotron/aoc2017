let add (x, y) (dx, dy) = (x + dx, y + dy)
let sub (x, y) (dx, dy) = (x - dx, y - dy)
let mul (x, y) a = (x * a, y * a)
let bit size (x, y) = 1 <<< ((y * size) + x)
let coords count = [0..count-1] |> List.collect (fun y -> [0..count-1] |> List.map (fun x -> x, y))
let coordsFrom offset count = coords count |> List.map (add offset)

let encode offset (size, bmp) =
  (size <<< 16) + (bmp |> Seq.map (fun x -> sub x offset) |> Seq.sumBy (bit size))

let decode offset key =
  let size = key >>> 16
  size, coords size |> Seq.filter (fun c -> key &&& bit size c <> 0) |> Seq.map (add offset) |> Set

let parseBitmap (str: string) =
  let size = str.IndexOf '/'
  let parseLine (y, (str: string)) =
    str |> Seq.indexed |> Seq.filter (snd >> (=) '#') |> Seq.map (fun (x, _) -> x, y) |> Set
  size, str.Split '/' |> Seq.indexed |> Seq.map parseLine |> Set.unionMany

let flipX (size, bmp) = size, bmp |> Set.map (fun (x, y) -> (size - 1) - x, y)
let flipY (size, bmp) = size, bmp |> Set.map (fun (x, y) -> x, (size - 1) - y)
let flipXY = flipX >> flipY
let rotateCW (size, bmp) = size, bmp |> Set.map (fun (x, y) -> y,  (size - 1) - x)
let rotateCCW (size, bmp) = size, bmp |> Set.map (fun (x, y) -> (size - 1) - y, x)
let mutations bmp =
  [bmp; rotateCW bmp; rotateCCW bmp] |> List.collect (fun x -> [x; flipX x; flipY x; flipXY x])

let foldRule (rules: Map<int, int>) (str: string) =
  let split = str.Split " => "
  let lhs = parseBitmap split.[0]
  let rhs = parseBitmap split.[1]
  mutations lhs |> List.fold (fun rules bmp -> rules |> Map.add (bmp |> encode (0, 0)) (rhs |> encode (0, 0))) rules

let rules = System.IO.File.ReadAllLines "day-21-input.txt" |> Array.fold foldRule Map.empty

let getChunkKey offset chunkSize bmp =
  let bmp = coordsFrom offset chunkSize |> Seq.filter (fun x -> bmp |> Set.contains x)
  encode offset (chunkSize, bmp)

let enhanceBlocks chunkSize (size, bmp) =
  let chunks =
    coords (size / chunkSize)
    |> Seq.map (fun x -> x, bmp |> getChunkKey (mul x chunkSize) chunkSize)
    |> Map
  let recombine combined (pos, key) =
    let (_, newChunk) = key |> decode (mul pos (chunkSize + 1))
    newChunk |> Seq.fold (fun s p -> s |> Set.add p) combined
  let newSize = (chunkSize + 1) * (size / chunkSize)
  newSize, chunks |> Map.map (fun _ v -> rules.[v]) |> Map.toSeq |> Seq.fold recombine Set.empty

let enhance (size, bmp) =
  enhanceBlocks (if size % 2 = 0 then 2 else 3) (size, bmp)

let repeat fn n = List.init n (fun _ -> fn) |> List.reduce (>>)
let startingPattern = (3, Set [0, 2; 1, 0; 1, 2; 2, 1; 2, 2])
let pixelCount (_, bmp) = bmp |> Set.count

printfn "Part 1: %d" (startingPattern |> repeat enhance 5 |> pixelCount)
printfn "Part 2: %d" (startingPattern |> repeat enhance 18 |> pixelCount)
