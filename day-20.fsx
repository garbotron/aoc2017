open System.Text.RegularExpressions

type Vec = int64 * int64 * int64
type Particle = { Position: Vec; Velocity: Vec; Acceleration: Vec }

let parseParticle (str: string) =
  let term = "<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>"
  let m = Regex.Match(str, "p=" + term + ", v=" + term + ", a=" + term)
  let num (i: int) = m.Groups.[i].Value |> int64
  { Position = num 1, num 2, num 3
    Velocity = num 4, num 5, num 6
    Acceleration = num 7, num 8, num 9 }

let input =
  System.IO.File.ReadAllLines "day-20-input.txt"
  |> Array.toList
  |> List.map parseParticle

let add (x, y, z) (dx, dy, dz) = x + dx, y + dy, z + dz
let dist (x, y, z) = abs x + abs y + abs z

let update p =
  let p = { p with Velocity = add p.Velocity p.Acceleration }
  { p with Position = add p.Position p.Velocity }

let afterManyUpdates = [0..1000] |> Seq.fold (fun p _ -> p |> List.map update) input
let closest = afterManyUpdates |> List.indexed |> List.minBy (fun (_, x) -> dist x.Position)
printfn "Part 1: %d" (fst closest)

let rec runWithCollisions rounds particles =
  let particles =
    particles
    |> List.map update
    |> List.groupBy (fun x -> x.Position)
    |> List.map snd
    |> List.filter (fun x -> x.Length = 1)
    |> List.map (List.head)
  match rounds with
  | 0 -> particles
  | x -> particles |> runWithCollisions (x - 1)

printfn "Part 2: %d" (input |> runWithCollisions 1000 |> List.length)
