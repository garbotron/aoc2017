let numbers =
  System.IO.File.ReadAllText "day-01-input.txt"
  |> fun x -> x.Trim()
  |> Seq.toList
  |> List.map (string >> int)

let isValid skip i = numbers.[i] = numbers.[(i + skip) % numbers.Length]
let result skip = [0..numbers.Length-1] |> List.filter (isValid skip) |> List.sumBy (fun x -> numbers.[x])

printfn "Part 1: %d" (result 1)
printfn "Part 2: %d" (result (numbers.Length / 2))
