let parseLine (str: string) = str.Split ' ' |> Array.toList
let input = System.IO.File.ReadAllLines "day-04-input.txt" |> Array.toList |> List.map parseLine
let isValid passphrase = (passphrase |> Set |> Set.count) = (passphrase |> List.length)
let normalizeWord (str: string) = str |> Seq.sort |> Seq.toArray |> System.String
let normalizeWords passphrase = passphrase |> List.map normalizeWord

printfn "Part 1: %d" (input |> Seq.filter isValid |> Seq.length)
printfn "Part 2: %d" (input |> List.map normalizeWords |> Seq.filter isValid |> Seq.length)
