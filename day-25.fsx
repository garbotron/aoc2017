open System.Text.RegularExpressions

type Action = {
  Condition: bool
  Write: bool
  Move: int
  NewState: char
}

type State = {
  ID: char
  Actions: Map<bool, Action>
}

type TM = {
  Tape: Map<int, bool>
  Position: int
  States: Map<char, State>
  DiagLength: int
  CurrentState: char
  Steps: int
}

let inputRegexLines =
  [ "Begin in state (?<sstate>[A-Z])."
    "Perform a diagnostic checksum after (?<diag>[0-9]+) steps."
    "("
    "In state (?<instate>[A-Z]):"
    "(  If the current value is (?<condition>0|1):"
    "    - Write the value (?<write>0|1)."
    "    - Move one slot to the (?<move>left|right)."
    "    - Continue with state (?<next>[A-Z])."
    ")+)+" ]

let inputRegex = inputRegexLines |> List.reduce (sprintf "%s\n%s")
let parseInput (str: string) =
  let m = Regex.Match(str, inputRegex)
  if not m.Success then failwith "regex match failed"
  let capture (name: string) idx = m.Groups.[name].Captures.[idx].Value

  let parseAction idx =
    { Condition = capture "condition" idx = "1"
      Write = capture "write" idx = "1"
      Move = if capture "move" idx = "left" then -1 else 1
      NewState = capture "next" idx |> Seq.head }

  let parseState idx =
    { ID = capture "instate" idx |> Seq.head
      Actions = [idx * 2; idx * 2 + 1] |> List.map parseAction |> List.map (fun x -> x.Condition, x) |> Map }

  let stateCount = m.Groups.["instate"].Captures.Count
  { Tape = Map.empty
    Position = 0
    CurrentState = capture "sstate" 0 |> Seq.head
    DiagLength = capture "diag" 0 |> int
    States = [0..stateCount-1] |> List.map parseState |> List.map (fun x -> x.ID, x) |> Map
    Steps = 0 }

let step tm =
  let state = tm.States |> Map.find tm.CurrentState
  let cur = tm.Tape |> Map.tryFind tm.Position |> Option.defaultValue false
  let action = state.Actions |> Map.find cur
  { tm with
      Tape = tm.Tape |> Map.add tm.Position action.Write
      Position = tm.Position + action.Move
      CurrentState = action.NewState
      Steps = tm.Steps + 1 }

let rec runDiag tm =
  if tm.Steps = tm.DiagLength then
    tm
  else
    tm |> step |> runDiag

let checksum tm = tm.Tape |> Map.filter (fun _ v -> v) |> Map.count

let input = System.IO.File.ReadAllText "day-25-input.txt" |> parseInput

printfn "Part 1: %d" (input |> runDiag |> checksum)
