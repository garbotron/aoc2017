open System.Text.RegularExpressions

type Instruction = {
  Operator: int -> int -> int
  Register: string
  Operand: int
  Condition: int -> int -> bool
  ConditionRegister: string
  ConditionOperand: int
}

type Cpu = {
  Regs: Map<string, int>
  HighestValue: int
}

let parseInstruction (str: string) =
  let m = Regex.Match(str, "([a-z]+) (inc|dec) (-?[0-9]+) if ([a-z]+) ([^ ]+) (-?[0-9]+)")
  { Register = m.Groups.[1].Value
    Operator = if m.Groups.[2].Value = "inc" then (+) else (-)
    Operand = int m.Groups.[3].Value
    ConditionRegister = m.Groups.[4].Value
    Condition =
      match m.Groups.[5].Value with
      | "==" -> (=)
      | "!=" -> (<>)
      | "<" -> (<)
      | "<=" -> (<=)
      | ">" -> (>)
      | ">=" -> (>=)
      | _ -> raise <| System.Exception()
    ConditionOperand = int m.Groups.[6].Value
  }

let input = System.IO.File.ReadAllLines "day-08-input.txt" |> Array.toList |> List.map parseInstruction

let run cpu instr =
  let cur = cpu.Regs |> Map.tryFind instr.ConditionRegister |> Option.defaultValue 0
  if instr.Condition cur instr.ConditionOperand then
    let oldVal = cpu.Regs |> Map.tryFind instr.Register |> Option.defaultValue 0
    let newVal = instr.Operator oldVal instr.Operand
    { cpu with
        Regs = cpu.Regs |> Map.add instr.Register newVal
        HighestValue = max cpu.HighestValue newVal }
  else
    cpu

let afterRun = input |> List.fold run { Regs = Map.empty; HighestValue = 0 }
printfn "Part 1: %d" (afterRun.Regs |> Map.toSeq |> Seq.map snd |> Seq.max)
printfn "Part 2: %d" afterRun.HighestValue
