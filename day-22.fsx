type Node = Clean | Weakened | Infected | Flagged

type State = {
  Cur: int * int
  Heading: int * int
  Nodes: Map<int * int, Node>
  InfectionsCaused: int
}

let initialNodes (lines: string[]) =
  let yOffset = lines.Length / 2
  let xOffset = lines.[0].Length / 2
  let foldLine nodes (y, str) =
    let foldChar nodes (x, ch) =
      if ch = '#' then nodes |> Map.add (x - xOffset, y - yOffset) Infected else nodes
    str |> Seq.indexed |> Seq.fold foldChar nodes
  lines |> Seq.indexed |> Seq.fold foldLine Map.empty

let initialState = {
  Cur = (0, 0)
  Heading = (0, -1)
  Nodes = System.IO.File.ReadAllLines "day-22-input.txt" |> initialNodes
  InfectionsCaused = 0
}

let add (x, y) (dx, dy) = (x + dx, y + dy)
let turnLeft (hx, hy) = (hy, -hx)
let turnRight (hx, hy) = (-hy, hx)
let turnAround (hx, hy) = (-hx, -hy)

let p1behavior = function
  | Clean -> turnLeft, Infected
  | Infected -> turnRight, Clean
  | _ -> failwith "bad node state"

let p2behavior = function
  | Clean -> turnLeft, Weakened
  | Weakened -> id, Infected
  | Infected -> turnRight, Flagged
  | Flagged -> turnAround, Clean

let burst behavior state =
  let (turn, node) = behavior (state.Nodes |> Map.tryFind state.Cur |> Option.defaultValue Clean)
  let heading = state.Heading |> turn
  { state with
      Heading = heading
      Cur = add state.Cur heading
      Nodes = state.Nodes |> Map.add state.Cur node
      InfectionsCaused = state.InfectionsCaused + (if node = Infected then 1 else 0) }

let bursts count behavior state =
  {1..count} |> Seq.fold (fun x _ -> x |> burst behavior) state

printfn "Part 1: %d" (initialState |> bursts 10000 p1behavior |> fun x -> x.InfectionsCaused)
printfn "Part 2: %d" (initialState |> bursts 10000000 p2behavior |> fun x -> x.InfectionsCaused)
