type Tile = Blank | Straight | Turn | Letter of char

let foldLine state (y, str) =
  let parseChar = function
    | ' ' -> Blank
    | '|' | '-' -> Straight
    | '+' -> Turn
    | x when x >= 'A' && x <= 'Z' -> Letter x
    | _ -> raise <| System.Exception()
  let foldChar state (x, c) = state |> Map.add (x, y) (parseChar c)
  str |> Seq.indexed |> Seq.fold foldChar state

let input =
  System.IO.File.ReadAllLines "day-19-input.txt"
  |> Seq.indexed
  |> Seq.fold foldLine Map.empty

let tile pos = input |> Map.tryFind pos |> Option.defaultValue Blank
let start = input |> Map.filter (fun (_, y) t -> y = 0 && t = Straight) |> Map.toSeq |> Seq.head |> fst
let add (x, y) (dx, dy) = x + dx, y + dy
let inv (x, y) = -x, -y

let rec travel heading pos =
  let addStep (count, letters) = (count + 1, letters)
  let addLetter l (count, letters) = (count, l :: letters)
  match tile pos with
  | Straight -> travel heading (add pos heading) |> addStep
  | Letter x -> travel heading (add pos heading) |> addStep |> addLetter x
  | Turn ->
    let isValid heading = tile (add pos heading) <> Blank
    let newHeading = [-1, 0; 1, 0; 0, -1; 0, 1] |> List.find (fun x -> x <> inv heading && isValid x)
    travel newHeading (add pos newHeading) |> addStep
  | Blank -> 0, [] // stop

let (steps, letters) = travel (0, 1) start
printfn "Part 1: %s" (letters |> List.map string |> List.reduce (+))
printfn "Part 2: %d" steps
