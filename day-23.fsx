type Value = Literal of int64 | RegisterRef of char
type Instruction =
  | Set of char * Value
  | Sub of char * Value
  | Mul of char * Value
  | Jnz of Value * Value

type Cpu = {
  Registers: Map<char, int64>
  Program: Instruction list
  PC: int
  MulCount: int
}

let parseInstruction (str: string) =
  let words = str.Split ' '
  let charAt idx = words.[idx].[0]
  let numAt idx = words.[idx] |> int64
  let valueAt idx =
    if words.[idx].[0] |> System.Char.IsLetter then
      RegisterRef (charAt idx)
    else
      Literal (numAt idx)

  match words.[0] with
  | "set" -> Set (charAt 1, valueAt 2)
  | "sub" -> Sub (charAt 1, valueAt 2)
  | "mul" -> Mul (charAt 1, valueAt 2)
  | "jnz" -> Jnz (valueAt 1, valueAt 2)
  | _ -> raise <| System.Exception()

let input = System.IO.File.ReadAllLines "day-23-input.txt" |> Array.toList |> List.map parseInstruction
let initialCpu = { Registers = Map.empty; Program = input; PC = 0; MulCount = 0 }

let getReg reg cpu = cpu.Registers |> Map.tryFind reg |> Option.defaultValue 0L
let setReg reg v cpu = { cpu with Registers = cpu.Registers |> Map.add reg v }
let resolve cpu = function | Literal v -> v | RegisterRef r -> cpu |> getReg r
let step cpu = { cpu with PC = cpu.PC + 1 }

let runCycle cpu =
  match (cpu.Program |> List.item cpu.PC) with
  | Set (x, y) -> cpu |> setReg x (resolve cpu y) |> step
  | Sub (x, y) -> cpu |> setReg x (getReg x cpu - resolve cpu y) |> step
  | Mul (x, y) -> { cpu with MulCount = cpu.MulCount + 1 } |> setReg x (getReg x cpu * resolve cpu y) |> step
  | Jnz (x, _) when resolve cpu x = 0L -> cpu |> step
  | Jnz (_, y) -> { cpu with PC = cpu.PC + int (resolve cpu y) }

let isDead cpu =
  cpu.PC < 0 || cpu.PC >= cpu.Program.Length

let rec run cpu =
  if isDead cpu then cpu else cpu |> runCycle |> run

let after = initialCpu |> run
printfn "Part 1: %d" after.MulCount

let formatValue = function
  | Literal x -> string x
  | RegisterRef x -> string x

let formatInstruction = function
  | Set (x, y) -> sprintf "%c := %s" x (formatValue y)
  | Sub (x, Literal y) when y < 0L -> sprintf "%c += %d" x -y
  | Sub (x, y) -> sprintf "%c -= %s" x (formatValue y)
  | Mul (x, y) -> sprintf "%c *= %s" x (formatValue y)
  | Jnz (x, y) -> sprintf "if (%s != 0) jump %s" (formatValue x) (formatValue y)

(*
Part 2 is an offline analysis problem.
Original code (pretty-printed):
input |> List.map formatInstruction |> List.iter (printfn "%s")

b := 93
c := b
if (a != 0) jump 2
if (1 != 0) jump 5
b *= 100
b += 100000
c := b
c += 17000
f := 1
d := 2
e := 2
g := d
g *= e
g -= b
if (g != 0) jump 2
f := 0
e += 1
g := e
g -= b
if (g != 0) jump -8
d += 1
g := d
g -= b
if (g != 0) jump -13
if (f != 0) jump 2
h += 1
g := b
g -= c
if (g != 0) jump 2
if (1 != 0) jump 3
b += 17
if (1 != 0) jump -23

The section that is activated when a=1 is:

b *= 100
b += 100000
c := b
c += 17000

In the default case, the initial value of b and c are 93.
In the a=1 case, b is 109300 and c is 126300.

Further simplifying the psuedocode...
b := 109300
c := 126300
while (1) {
  f := 1
  d := 2
  do {
    e := 2
    do {
      g := d
      g *= e
      g -= b
      if (g == 0) {
        f := 0
      }
      e++
      g := e
      g -= b
    } while (g != 0)
    d++
    g := d
    g -= b
  } while (g != 0)
  if (f == 0) {
    h++
  }
  g := b
  g -= c
  if (g == 0) {
    break
  }
  b += 17
}

And a couple more steps...
b := 109300
c := 126300
while (1) {
  f := 1
  d := 2
  do {
    e := 2
    do {
      if (b == d * e) {
        f := 0
      }
      e++
    } while (e != b)
    d++
  } while (d != b)
  if (f == 0) {
    h++
  }
  if (b == c) {
    break
  }
  b += 17
}

And then even further...
for b in [109300..17..126300] {
  f := 1

  for d in [2..b] {
    for e in [2..b] {
      if (b == d * e) {
        f := 0
      }
    }
  }

  if (f == 0) {
    h++
  }
}


This code is looping through [109300..126300] range in increments of 17, looking to see if each number is prime.
If the number is non-prime, f will be set to 0, which will cause h to increment.
So the final result of h should be the count of non-primes in that range.
*)

let isNonPrime n = {2..n/2} |> Seq.exists (fun x -> n % x = 0)
printfn "Part 2: %d" ([109300..17..126300] |> Seq.filter isNonPrime |> Seq.length)
