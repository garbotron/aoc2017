let foldLine (state: Map<int, int list>) (str: string) =
  let split = str.Split " <-> "
  let lhs = int split.[0]
  let rhs = split.[1].Split ", " |> Array.toList |> List.map int
  state |> Map.add lhs rhs

let connectedPrograms program state =
  let mutable seen = Set.empty
  let rec explore p =
    if seen |> Set.contains p then
      []
    else
      seen <- seen |> Set.add p
      let connected = state |> Map.find p |> List.collect explore
      p :: connected
  explore program |> Set

let unfoldGroups state =
  if state |> Map.isEmpty then
    None
  else
    let head = state |> Map.toSeq |> Seq.head |> fst
    let group = state |> connectedPrograms head
    let rest = state |> Map.filter (fun x _ -> not (group |> Set.contains x))
    Some (group, rest)

let input = System.IO.File.ReadAllLines "day-12-input.txt" |> Array.fold foldLine Map.empty

printfn "Part 1: %d" (input |> connectedPrograms 0 |> Set.count)
printfn "Part 2: %d" (List.unfold unfoldGroups input |> List.length)
