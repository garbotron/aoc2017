type Thing = Garbage of int | Group of Thing list

let rec stripEscapeChars (str: char list) =
  match str with
  | [] -> []
  | '!' :: _ :: rest -> stripEscapeChars rest
  | x :: rest -> x :: stripEscapeChars rest

let rec parse chars =
  match chars with
  | '<' :: rest ->
    let garbage = rest |> Seq.takeWhile ((<>) '>') |> Seq.length
    Some (Garbage garbage, rest |> List.skip (garbage + 1))
  | '{' :: rest ->
    let (things, rest) = parseMany rest
    Some (Group things, rest |> List.tail)
  | ',' :: rest -> parse rest
  | _ -> None

and parseMany chars =
  match parse chars with
  | None -> [], chars
  | Some (thing, rest) ->
    let (things, rest) = parseMany rest
    thing :: things, rest

let rec score cur = function
  | Garbage _ -> 0
  | Group x -> cur + (x |> List.sumBy (score (cur + 1)))

let rec garbageCount = function
  | Garbage x -> x
  | Group x -> x |> List.sumBy garbageCount

let input = System.IO.File.ReadAllText "day-09-input.txt" |> fun x -> x.Trim() |> Seq.toList
let outermostGroup = input |> stripEscapeChars |> parse |> Option.get |> fst

printfn "Part 1: %d" (outermostGroup |> score 1)
printfn "Part 2: %d" (outermostGroup |> garbageCount)
