let input = System.IO.File.ReadAllLines "day-05-input.txt" |> Array.map int |> Array.indexed |> Map

// Sadly the functional solution hit a stack overflow, so here we go...
let run map update =
  let mutable pc = 0
  let mutable map = map
  let mutable iters = 0
  while map |> Map.containsKey pc do
    let jump = map |> Map.find pc
    map <- map |> Map.add pc (update jump)
    pc <- pc + jump
    iters <- iters + 1
  iters

printfn "Part 1: %d" (run input ((+) 1))
printfn "Part 2: %d" (run input (fun x -> if x >= 3 then x - 1 else x + 1))
