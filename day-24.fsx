let parseComponent (str: string) =
  let split = str.Split '/'
  int split.[0], int split.[1]

let input = System.IO.File.ReadAllLines "day-24-input.txt" |> Array.toList |> List.map parseComponent |> Set

let strength bridge = bridge |> List.sumBy (fun (x, y) -> x + y)
let lengthThenStrength bridge = bridge |> List.length, strength bridge

let rec bestBridge metric cur components =
  let connectedToA = components |> Set.filter (fst >> (=) cur)
  let connectedToB = components |> Set.filter (snd >> (=) cur)
  if connectedToA |> Set.isEmpty && connectedToB |> Set.isEmpty then
    []
  else
    let extendWith side c = c :: (components |> Set.remove c |> bestBridge metric (side c))
    Seq.maxBy metric <| Seq.concat [
      (connectedToA |> Seq.map (extendWith snd))
      (connectedToB |> Seq.map (extendWith fst))]

printfn "Part 1: %d" (input |> bestBridge strength 0 |> strength)
printfn "Part 2: %d" (input |> bestBridge lengthThenStrength 0 |> strength)
