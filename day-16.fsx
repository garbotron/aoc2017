type DanceMove = Spin of int | Exchange of int * int | Partner of char * char

let parseDanceMove (str: string) =
  match str.[0] with
  | 's' -> str.Substring 1 |> int |> Spin
  | 'x' ->
    let split = (str.Substring 1).Split '/'
    Exchange ((int split.[0]), (int split.[1]))
  | 'p' -> Partner (str.[1], str.[3])
  | _ -> raise <| System.Exception()

let input =
  System.IO.File.ReadAllText("day-17-input.txt")
  |> fun x -> x.Trim().Split ','
  |> Array.toList
  |> List.map parseDanceMove

let dance state move =
  match move with
  | Spin by ->
    // Everything (len - x) and after has (len - x) subtracted from its position. Others have x added.
    let rotate v =
      let len = state |> Map.count
      if v >= (len - by) then
        v - (len - by)
      else
        v + by

    state
    |> Map.toSeq
    |> Seq.map (fun (x, y) -> rotate x, y)
    |> Map

  | Exchange (x, y) ->
    let a = state |> Map.find x
    let b = state |> Map.find y
    state |> Map.add y a |> Map.add x b

  | Partner (x, y) ->
    let a = state |> Map.toSeq |> Seq.find (snd >> (=) x) |> fst
    let b = state |> Map.toSeq |> Seq.find (snd >> (=) y) |> fst
    state |> Map.add a y |> Map.add b x

let stateLabel state =
  state |> Map.toSeq |> Seq.sortBy fst |> Seq.map snd |> Seq.toArray |> System.String

let fullDance state = input |> List.fold dance state

let initialState = ['a'..'p'] |> List.indexed |> Map

printfn "Part 1: %s" (initialState |> fullDance |> stateLabel)

let rec itersUntilDuplicate state cache =
  if cache |> Set.contains state then
    0
  else
    1 + itersUntilDuplicate (state |> fullDance) (cache |> Set.add state)

let cycleLen = itersUntilDuplicate initialState Set.empty
let iters = 1000000000 % cycleLen
let afterABillion = [1..iters] |> List.fold (fun x _ -> x |> fullDance) initialState
printfn "Part 2: %s" (afterABillion |> stateLabel)
