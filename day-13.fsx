(*
  Each firewall will hit the 0 depth with the period ((range - 1) * 2).
  There will be a collision on each firewall if: depth % period == 0.
  To delay for part 2, increase all depths by n.
*)

let foldLine (firewalls: Map<int, int>) (str: string) =
  let split = str.Split ": "
  firewalls |> Map.add (int split.[0]) (int split.[1])

let input = System.IO.File.ReadAllLines "day-13-input.txt" |> Array.fold foldLine Map.empty

let findCollisions delay =
  let collide (depth, range) =
    if depth % ((range - 1) * 2) = 0 then
      Some (depth, range)
    else
      None
  input
  |> Map.toSeq
  |> Seq.map (fun (x, y) -> x + delay, y)
  |> Seq.choose collide
  |> Seq.toList

printfn "Part 1: %d" (findCollisions 0 |> List.sumBy (fun (x, y) -> x * y))
printfn "Part 2: %d" (Seq.initInfinite id |> Seq.find (fun x -> findCollisions x |> List.isEmpty))
