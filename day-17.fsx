type Node = { Value: int; mutable Next: Node; mutable Prev: Node }

let initialLL () =
  let rec init = { Value = 0; Next = init; Prev = init }
  init

let rec next n node =
  match n with
  | 0 -> node
  | x -> node.Next |> next (x - 1)

let rec spinlock insert skip last node =
  if insert > last then
    node
  else
    let insertAfter = node |> next skip
    let newNode = { Value = insert; Prev = insertAfter; Next = insertAfter.Next }
    insertAfter.Next.Prev <- newNode
    insertAfter.Next <- newNode
    newNode |> spinlock (insert + 1) skip last

let inputSkip = 354
printfn "Part 1: %d" (initialLL () |> spinlock 1 inputSkip 2017 |> fun x -> x.Next.Value)

let finalState = initialLL () |> spinlock 1 inputSkip 50000000
let zeroNode = finalState |> Seq.unfold (fun n -> Some (n.Next, n.Next)) |> Seq.find (fun x -> x.Value = 0)
printfn "Part 2: %d" zeroNode.Next.Value
