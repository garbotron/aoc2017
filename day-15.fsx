let generator factor start =
  let unfolder x =
    let next = (x * factor) % 2147483647L
    Some (next, next)
  Seq.unfold unfolder start

let generatorA = generator 16807L 703L
let generatorB = generator 48271L 516L

let low16match (a, b) = (a &&& 0xFFFFL) = (b &&& 0xFFFFL)

printfn "Part 1: %d" (Seq.zip generatorA generatorB |> Seq.take 40000000 |> Seq.filter low16match |> Seq.length)

let generatorA' = generatorA |> Seq.filter (fun x -> x % 4L = 0L)
let generatorB' = generatorB |> Seq.filter (fun x -> x % 8L = 0L)

printfn "Part 2: %d" (Seq.zip generatorA' generatorB' |> Seq.take 5000000 |> Seq.filter low16match |> Seq.length)
