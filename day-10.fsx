open System.Collections.Generic

let createList len = [0..len-1] |> List

let reverseSubsection (start: int) (len: int) (lst: List<int>) =
  for i in [0..(len-1)/2] do
    let a = (start + i) % lst.Count
    let b = ((start + len - 1) - i) % lst.Count
    let temp = lst.[a]
    lst.[a] <- lst.[b]
    lst.[b] <- temp
  lst

let rec round (lengths: int list) (skipSize: int) (cur: int) (list: List<int>) =
  match lengths with
  | [] -> list, skipSize, cur
  | x :: rest -> list |> reverseSubsection cur x |> round rest (skipSize + 1) ((cur + x + skipSize) % list.Count)

let inputP1 =
  System.IO.File.ReadAllText "day-10-input.txt"
  |> fun x -> x.Trim().Split ','
  |> Array.toList
  |> List.map int

printfn "Part 1: %d" (createList 256 |> round inputP1 0 0 |> fun (l, _, _) -> l.[0] * l.[1])

let rec rounds (lengths: int list) (skipSize: int) (cur: int) (count: int) (list: List<int>) =
  match count with
  | 0 -> list
  | x ->
    let (list, skipSize, cur) = list |> round lengths skipSize cur
    list |> rounds lengths skipSize cur (count - 1)

let sparseHash lengths list = list |> rounds lengths 0 0 64 |> Seq.toList

let denseHash sparseHash =
  sparseHash |> List.chunkBySize 16 |> List.map (List.reduce (^^^))

let formatDenseHash denseHash =
  denseHash |> List.map (sprintf "%02x") |> List.reduce (+)

let inputP2 =
  System.IO.File.ReadAllText "day-10-input.txt"
  |> fun x -> x.Trim()
  |> Seq.toList
  |> List.map int
  |> fun x -> x @ [17; 31; 73; 47; 23]

printfn "Part 2: %s" (createList 256 |> sparseHash inputP2 |> denseHash |> formatDenseHash)
