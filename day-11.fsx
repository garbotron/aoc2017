type Dir = N | S | NE | NW | SE | SW

let parseDir = function
  | "n" -> N
  | "s" -> S
  | "ne" -> NE
  | "nw" -> NW
  | "se" -> SE
  | "sw" -> SW
  | _ -> raise <| System.Exception()

// Use "cube coordinates" ala https://www.redblobgames.com/grids/hexagons/.
let travel (x, y, z) = function
  | N -> (x, y + 1, z - 1)
  | S -> (x, y - 1, z + 1)
  | NE -> (x + 1, y, z - 1)
  | SW -> (x - 1, y, z + 1)
  | NW -> (x - 1, y + 1, z)
  | SE -> (x + 1, y - 1, z)

let input =
  System.IO.File.ReadAllText "day-11-input.txt"
  |> fun x -> x.Trim().Split ','
  |> Array.toList
  |> List.map parseDir

let dist (x, y, z) = [abs x; abs y; abs z] |> List.max

printfn "Part 1: %d" (input |> List.fold travel (0, 0, 0) |> dist)
printfn "Part 2: %d" (input |> List.scan travel (0, 0, 0) |> List.map dist |> List.max)
