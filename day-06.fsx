let input =
  System.IO.File.ReadAllText "day-06-input.txt"
  |> fun x -> x.Trim().Split(' ', '\t')
  |> Array.map int
  |> Array.indexed
  |> Map

let cycle banks =
  let next idx = (idx + 1) % (banks |> Map.count)
  let rec redistribute banks count idx =
    match count with
    | 0 -> banks
    | n -> redistribute (banks |> Map.add idx (banks.[idx] + 1)) (n - 1) (next idx)
  let (idx, count) = banks |> Map.toSeq |> Seq.maxBy (fun (i, v) -> v, -i)
  redistribute (banks |> Map.add idx 0) count (next idx)

let rec reallocate banks cache =
  if cache |> Set.contains banks then
    banks, 0
  else
    let (banks, count) = reallocate (banks |> cycle) (cache |> Set.add banks)
    banks, count + 1

let (firstDupMap, firstDupCount) = reallocate input Set.empty

printfn "Part 1: %d" firstDupCount
printfn "Part 2: %d" (reallocate firstDupMap Set.empty |> snd)
