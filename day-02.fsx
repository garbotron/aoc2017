let parseRow (str: string) =
  str.Split(' ', '\t') |> Array.toList |> List.map int

let input =
  System.IO.File.ReadAllLines "day-02-input.txt" |> Array.toList |> List.map parseRow

let rowChecksum row = (row |> List.max) - (row |> List.min)

let rowEvenDivision row =
  let pairs = row |> Seq.collect (fun x -> row |> Seq.except [x] |> Seq.map (fun y -> x, y))
  pairs |> Seq.find (fun (x, y) -> x % y = 0) |> fun (x, y) -> x / y

printfn "Part 1: %d" (input |> List.sumBy rowChecksum)
printfn "Part 2: %d" (input |> List.sumBy rowEvenDivision)
