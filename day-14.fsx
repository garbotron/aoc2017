open System.Collections.Generic

let knotHash (input: string) =
  let createList len = [0..len-1] |> List

  let reverseSubsection (start: int) (len: int) (lst: List<int>) =
    for i in [0..(len-1)/2] do
      let a = (start + i) % lst.Count
      let b = ((start + len - 1) - i) % lst.Count
      let temp = lst.[a]
      lst.[a] <- lst.[b]
      lst.[b] <- temp
    lst

  let rec round (lengths: int list) (skipSize: int) (cur: int) (list: List<int>) =
    match lengths with
    | [] -> list, skipSize, cur
    | x :: rest -> list |> reverseSubsection cur x |> round rest (skipSize + 1) ((cur + x + skipSize) % list.Count)

  let rec rounds (lengths: int list) (skipSize: int) (cur: int) (count: int) (list: List<int>) =
    match count with
    | 0 -> list
    | x ->
      let (list, skipSize, cur) = list |> round lengths skipSize cur
      list |> rounds lengths skipSize cur (count - 1)

  let sparseHash lengths list = list |> rounds lengths 0 0 64 |> Seq.toList

  let denseHash sparseHash =
    sparseHash |> List.chunkBySize 16 |> List.map (List.reduce (^^^))

  let formatDenseHash denseHash =
    denseHash |> List.map (sprintf "%02x") |> List.reduce (+)

  let input = (input |> Seq.toList |> List.map int) @ [17; 31; 73; 47; 23]
  createList 256 |> sparseHash input |> denseHash

let hashResultToBools result =
  let convertSection x = [7..-1..0] |> List.map (fun i -> (x &&& (1 <<< i)) <> 0)
  result |> List.collect convertSection

let input = "xlqgujun"
let rows = [0..127] |> List.map (sprintf "%s-%d" input >> knotHash >> hashResultToBools)

printfn "Part 1: %d" (rows |> List.collect id |> List.filter id |> List.length)

let findRegionCount (rows: (bool list) list) =
  let mutable regions = Map.empty
  let mutable nextRegionID = 0
  for y in [0..127] do
    for x in [0..127] do
      if rows.[y].[x] then
        let up = regions |> Map.tryFind (x, y - 1)
        let left = regions |> Map.tryFind (x - 1, y)
        match (up, left) with
        | Some n, None -> regions <- regions |> Map.add (x, y) n
        | None, Some n -> regions <- regions |> Map.add (x, y) n
        | None, None ->
          // New region, not yet connected to anything.
          regions <- regions |> Map.add (x, y) nextRegionID
          nextRegionID <- nextRegionID + 1
        | Some n, Some m ->
          // Connected to 2 regions. If they are different, merge them into a single region.
          regions <- regions |> Map.map (fun _ v -> if v = n then m else v) |> Map.add (x, y) m
  regions |> Map.toSeq |> Seq.map snd |> Seq.distinct |> Seq.length

printfn "Part 2: %d" (rows |> findRegionCount)
